﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;

namespace JavaScriptPageInjector
{
    class MatchCompairer : IEqualityComparer<Match>
    {
        public bool Equals([AllowNull] Match x, [AllowNull] Match y)
        {
            if(x != null && y != null)
            {
                string xvalue = x.Groups[1].Value;
                string yvalue = y.Groups[1].Value;
                if (xvalue.IndexOf('/') == 0)
                {
                    xvalue = xvalue.Substring(1);
                }
                if (yvalue.IndexOf('/') == 0)
                {
                    yvalue = yvalue.Substring(1);
                }
                return xvalue == yvalue;
            }
            else
            {
                return false;
            }
        }

        public int GetHashCode([DisallowNull] Match obj)
        {
            return 111;
        }
    }
}
