﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JavaScriptPageInjector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Regex RegexJS;
        Regex RegexCss;
        Regex RegexjsName;
        Regex RegexCssName;
        Regex Regexjspath;
        Regex Regexcsspath;
        Timer timer;
        Action<List<string>, bool,RichTextBox> appOutputAction;
        private readonly object timerlock = new object();
        public MainWindow()
        {
            InitializeComponent();
            appOutputAction = (texts, startover,richTextBox) =>
            {
                if (startover)
                {
                    richTextBox.Document.Blocks.Clear();
                }
                foreach(string text in texts)
                {
                    richTextBox.Document.Blocks.Add(new Paragraph(new Run(text)));
                }
            };
        }
        private void Extract_Js_Code(object sender, RoutedEventArgs e)
        {
            ExtractCssAndJsAsync(txtInput);
        }
        private async void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            await Task.Run(() =>
            {
                try
                {
                    if (!new TextRange(txtInput.Document.ContentStart, txtInput.Document.ContentEnd).Text.StartsWith("input:"))
                    {
                        if (!(timer is null))
                        {
                            timer.Stop();
                            timer.Start();
                        }
                        else
                        {
                            lock (timerlock)
                            {
                                if (timer is null)
                                {
                                    timer = new Timer(200);
                                    timer.Enabled = true;
                                    timer.AutoReset = false;
                                    timer.Elapsed += async (sender, e) =>
                                    {
                                        await ExtractCssAndJsAsync(txtInput);
                                    };
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {

                }
               
            });
        }
        public async Task ExtractCssAndJsAsync(RichTextBox richTextBox = null , ExtracResault extracResault = null)
        {
            //var mainwindow = this;
            string originalText = string.Empty;
            if (extracResault != null)
            {
                originalText = extracResault.OriginalAspxText;
            }
            if(richTextBox != null)
            {
                originalText = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd).Text;
            }
            await Task.Run(() =>
            {
                RegexJS = new Regex("<script[-_a-z A-Z\\=\\/\"0-9]*src=\"([-_a-z A-z.\\/0-9]*)\"[-_a-z A-Z0-9\\=\\/\"><|?&]*\\/script>"); 
                                    //<script[-_a-z A-Z\=\/\"0-9]*src="([-_a-zA-z .\/0-9]*)"[-_a-z A-Z0-9\=\/\"><|?&\\]*\/script>
                RegexCss = new Regex("<link[-_a-z A-Z\\=\\/\"0-9]*href=\"([-_a-z A-z.\\/0-9]*)\"[-_a-z A-Z0-9\\=\\/\"><|?&]*\\/>");
                                    //<link[-_a-z A-Z\=\/\"0-9]*href="([-_a-zA-z .\/0-9]*)"[-_a-z A-Z0-9\=\/\"><|?&\\]*\/>
                RegexjsName = new Regex("\\/([a-z A-Z.0-9-_]*)\\.js");
                                         //\/([a-z A-Z.0-9-_]*)\.js
                RegexCssName = new Regex("\\/([a-z A-Z.0-9-_]*)\\.css");
                                         //\/([a-z A-Z.0-9]*)\.css
                Regexcsspath = new Regex("[\"'](.*)\\.css[\"']");
                                        //["'](.*)\.css["']
                Regexjspath = new Regex("[\"'](.*)\\.js[\"']");
                                        //["'](.*)\.js["']
                List<string> JsNames = new List<string>();
                List<string> appoutputlist = new List<string>();
                MatchCompairer matchCompairer = new MatchCompairer();
                MatchCollection jsmatches = RegexJS.Matches(originalText);
                MatchCollection cssmatches = RegexCss.Matches(originalText);
                MatchCollection jsnamesmatches = RegexjsName.Matches(originalText);
                MatchCollection cssnamematches = RegexCssName.Matches(originalText);
                if(extracResault != null)
                {
                    extracResault.JsFileCountMatched = jsmatches.Count == Regexjspath.Matches(originalText).Count;
                    extracResault.CssFileCountMatched = cssmatches.Count == Regexcsspath.Matches(originalText).Count;
                    extracResault.TotalCssFiles = cssmatches.Count;
                    extracResault.TotalJsFiles = jsmatches.Count;
                }
                appoutputlist.Add($"Total Css Matches: {cssmatches.Count} \nTotal JS Matches {jsmatches.Count}");
                if(jsnamesmatches.Count > 0)
                {
                    string jsnameout  = $"JS Names:\n";
                    foreach(Match jsnamematch in jsnamesmatches)
                    {
                        jsnameout += $"{jsnamematch.Groups[1].Value}.js\n";
                    }
                    appoutputlist.Add(jsnameout);
                }
                if (cssnamematches.Count > 0)
                {
                    string cssnameout = $"CSS Names:\n";
                    foreach (Match cssnamematch in cssnamematches)
                    {
                        cssnameout += $"{cssnamematch.Groups[1].Value}.css\n";
                    }
                    appoutputlist.Add(cssnameout);
                }
                if (richTextBox != null)
                {
                    Dispatcher.Invoke(appOutputAction, new object[3] { appoutputlist, true, txtappoutput });
                }
                string WebpackJsOutput = string.Empty;
                bool had = false;
                int index = 0;
                foreach (Match csmatch in cssmatches.Distinct(matchCompairer))
                {
                    index++;
                    cssmatches.Distinct();
                    string cssvalue = csmatch.Groups[1].Value;
                    cssvalue = cssvalue.Replace("../", "").Replace("./", "");
                    if (cssvalue.IndexOf('/') == 0)
                    {
                        cssvalue = cssvalue.Substring(1);
                    }
                    WebpackJsOutput += $"require('../../../../baseAssets/lms/{cssvalue}');\n";
                    had = true;
                }
                if(extracResault != null)
                {
                    extracResault.DistinctedCssCount = index;
                }
                if(!had)
                {
                    WebpackJsOutput += $"\n";
                    had = false;
                }
                WebpackJsOutput += $"\n//import Vue from 'vue';\n//import axios from 'axios';\n\n";
                index = 0;
                foreach (Match jsmatch in jsmatches.Distinct(matchCompairer))
                {
                    index++;
                    string jsvalue = jsmatch.Groups[1].Value;
                    jsvalue = jsvalue.Replace("../","").Replace("./","");
                    if(jsvalue.IndexOf('/') == 0)
                    {
                        jsvalue = jsvalue.Substring(1);
                    }
                    string jsfilename = RegexjsName.Match(jsvalue).Groups[1].Value.Replace('.', '_').Replace('-','_').ToLower();
                    if(jsfilename == "default")
                    {
                        jsfilename = $"the{jsfilename}".ToLower();
                    }
                    if(jsfilename == string.Empty)
                    {
                       jsfilename  = jsvalue.Replace(".js", "").Replace('.', '_').Replace('-', '_').ToLower();
                    }
                    JsNames.Add(jsfilename);
                    WebpackJsOutput += $"import {jsfilename} from '../../../../baseAssets/lms/{jsvalue}';\n";
                    had = true;
                }
                if(extracResault != null)
                {
                    extracResault.DistinctedJSCount = index;
                }
                if (!had)
                {
                    WebpackJsOutput += $"\n";
                    had = false;
                }
                WebpackJsOutput += $"\n//window.jQuery = $;\n//window.$ = $;\n//jQuery = $;\n\n";
                foreach(string name in JsNames)
                {
                    WebpackJsOutput += $"{name}();\n";
                }
                WebpackJsOutput += $"\n\n//window.store = store;\n//window.Vue = Vue;\n\n$( document ).ready( () =>\n{{\n\n}});";
                if (richTextBox != null)
                {
                    Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { WebpackJsOutput }, true, txtoutputone });
                }
                if(cssmatches.Count > 0)
                {
                    if (extracResault != null)
                    {
                        originalText = originalText.Replace(cssmatches.First().Groups[0].Value, $"<link rel=\"stylesheet\" type=\"text/css\" href=\"{extracResault.ExtractedCssFileName}\" />");
                    }
                    else
                    {
                        originalText = originalText.Replace(cssmatches.First().Groups[0].Value, "!!!!!////@@@@NavidCSSHEREdivaN@@@@\\\\\\\\!!!!!");
                    }
                }
                if(jsmatches.Count > 0)
                {
                    if (extracResault != null)
                    {
                        originalText = originalText.Replace(jsmatches.First().Groups[0].Value, $"<script type=\"text/javascript\" src=\"{extracResault.ExtractedJSFileName}\"></script>");
                    }
                    else
                    {
                        originalText = originalText.Replace(jsmatches.First().Groups[0].Value, "!!!!!////@@@@NavidJSHEREdivaN@@@@\\\\\\\\!!!!!");
                    }
                }
                foreach(Match jsmatch in jsmatches)
                {
                    originalText = originalText.Replace(jsmatch.Groups[0].Value, "");
                }
                foreach (Match cssmatch in cssmatches)
                {
                    originalText = originalText.Replace(cssmatch.Groups[0].Value, "");
                }
                if (richTextBox != null)
                {
                    Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { originalText }, true, txtoutputtwo });
                }
                if(extracResault != null)
                {
                    extracResault.UpdatedAspxText = originalText;
                    extracResault.WebpackJsCode = WebpackJsOutput;
                }
            });
        }
        private async void Generate_WebPack_Script_Path(object sender, RoutedEventArgs e)
        {
            await Task.Run(async () =>
            { 
               string[] coll = new TextRange(txtInput.Document.ContentStart, txtInput.Document.ContentEnd).Text.Split("\r\n");
               string input = coll[0].Substring(coll[0].IndexOf(":") + 1);
               string output = coll[1].Substring(coll[1].IndexOf(":") + 1);
               List<string> AspxFiles = new List<string>();
               List<ExtracResault> extracResaults = new List<ExtracResault>();
               List<Task> TasksToComplete = new List<Task>();
               if (Directory.Exists(input) && Directory.Exists(output))
               {
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "Directories existed Starting To Convert" }, true, txtappoutput });
                   var resault = Directory.GetFiles(input, "*.aspx");
                   if (resault.Length > 0)
                   {
                       AspxFiles.AddRange(resault);
                   }
                   var subdirectories = Directory.GetDirectories(input, string.Empty, SearchOption.AllDirectories);
                   foreach (var subdir in subdirectories)
                   {
                       resault = Directory.GetFiles(subdir, "*.aspx");
                       if (resault.Length > 0)
                       {
                           AspxFiles.AddRange(resault);
                       }
                   }
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { $"Found {AspxFiles.Count} Aspx Files." }, false, txtappoutput });
                   Dispatcher.Invoke(appOutputAction, new object[3] { AspxFiles, true, txtoutputtwo });
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "starting" }, true, txtoutputone });
                   int index = 0;
                   foreach (var aspxfile in AspxFiles)
                   {
                       TasksToComplete.Add(Task.Run(async () =>
                       {
                           string aspxfilename = aspxfile.Substring(aspxfile.LastIndexOf("\\") + 1);
                           string relative = aspxfile.Replace(input , "").Replace("\\\\","\\");
                           string extractFileName = relative.Remove(relative.LastIndexOf('.')).Substring(1).Replace("\\", "-");
                           relative = relative.Replace(aspxfilename, "");
                           FileStream fileStream = File.OpenRead(aspxfile);
                           StreamReader streamReader = new StreamReader(fileStream);
                           ExtracResault extracResault = new ExtracResault()
                           {
                               AspxFileFullPath = aspxfile,
                               AspxFileRelativePath = relative,
                               AspxFileName = aspxfilename,
                               ExtractedCssFileName = $"http://localhost:8080/dist/base-lms-{extractFileName}.css",//change
                               ExtractedJSFileName = $"http://localhost:8080/dist/base-lms-{extractFileName}.bundle.js",//change
                               WebpackViewRelativePath = $"\\ClientApp\\Views\\Base\\LMS\\{extractFileName}\\",//change
                               OriginalAspxText = await streamReader.ReadToEndAsync()
                           };
                           extracResaults.Add(extracResault);
                           await ExtractCssAndJsAsync(null, extracResault);            
                           if(extracResault.TotalJsFiles > 0)
                           {
                               index++;
                               Directory.CreateDirectory($"{output}\\webpackJSs{extracResault.WebpackViewRelativePath}");
                               Directory.CreateDirectory($"{output}\\aspxPages{extracResault.AspxFileRelativePath}");
                               FileStream webpackjsfile = File.OpenWrite($"{output}\\webpackJSs{extracResault.WebpackViewRelativePath}index.js");
                               FileStream outputAspxFile = File.OpenWrite($"{output}\\aspxPages{extracResault.AspxFileRelativePath}{extracResault.AspxFileName}");
                               StreamWriter streamWriterAspx = new StreamWriter(outputAspxFile);
                               StreamWriter streamWriterWebPackJS = new StreamWriter(webpackjsfile);
                               await streamWriterAspx.WriteAsync(extracResault.UpdatedAspxText);
                               await streamWriterAspx.FlushAsync();
                               streamWriterAspx.Close();
                               await streamWriterWebPackJS.WriteAsync(extracResault.WebpackJsCode);
                               await streamWriterWebPackJS.FlushAsync();
                               streamWriterWebPackJS.Close();
                           }   
                           Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { extracResault.ToString() }, false, txtoutputone });
                       }));
                   }
                   await Task.WhenAll(TasksToComplete);
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { $"{index} Of Js Files And ASPX File Created." }, false, txtappoutput });
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { $"Task Completed." }, false, txtappoutput });
               }
               else
               {
                   Dispatcher.Invoke(appOutputAction, new object[3] { new List<string>() { "The directory you just entered does not exist..." }, true, txtappoutput });
               }
           }); 
        }
    }
}
