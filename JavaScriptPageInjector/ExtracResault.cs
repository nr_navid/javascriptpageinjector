﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JavaScriptPageInjector
{
    public class ExtracResault
    {
        public string Extracrtoutput { get; set; }
        public bool JsFileCountMatched { get; set; }
        public bool CssFileCountMatched { get; set; }
        public int TotalJsFiles { get; set; }
        public int TotalCssFiles { get; set; }
        public int DistinctedJSCount { get; set; }
        public int DistinctedCssCount { get; set; }
        public string AspxFileFullPath { get; set; }
        public string AspxFileRelativePath { get; set; }
        public string AspxFileName { get; set; }
        public string OriginalAspxText { get; set; }
        public string UpdatedAspxText { get; set; }
        public string WebpackJsCode { get; set; }
        public string WebpackViewRelativePath { get; set; }
        public string ExtractedCssFileName { get; set; }
        public string ExtractedJSFileName { get; set; }
        public override string ToString()
        {
            return $"{JsFileCountMatched};{CssFileCountMatched};{TotalJsFiles};{DistinctedJSCount};{TotalCssFiles};{DistinctedCssCount};{ExtractedJSFileName};{ExtractedCssFileName};{AspxFileRelativePath};{AspxFileName};{WebpackViewRelativePath}";
        }
    }
}
